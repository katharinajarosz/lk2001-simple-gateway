package tcp;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.nio.charset.StandardCharsets;

public class NIOClient {

    private String ip;
    private int port;
    private SocketChannel clientSocket;

    public NIOClient(String ip, int port) {
        this.ip = ip;
        this.port = port;
    }

    public void getDataStream() { // run the service
        try {
            InetSocketAddress inetSocketAddress = new InetSocketAddress(ip, port);
            connectToServer(inetSocketAddress);
            firstMessage();
            int count = 0;

            while (true) {

                if(clientSocket.isConnected()) {
                    count++;
                    ByteBuffer bb = readProcessImageStream();
                    System.out.println("BYTE BUFFER: " + bb + " " + count);
                }

                if (count == 10) {
                    clientSocket.close();
                }

            }
        } catch (IOException ex) {
            System.out.println("--- Interrupt server.NetworkService-run");
            System.out.println("EXCEPTION: " + ex);

        } finally {
            System.out.println("--- Ende server.NetworkService(pool.shutdown)");
            try {
                if (!clientSocket.isConnected()) {
                    System.out.println("--- Ende server.NetworkService:ServerSocket close");
                    clientSocket.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void connectToServer(InetSocketAddress inetSocketAddress) throws IOException {
        clientSocket = SocketChannel.open(inetSocketAddress);
        writeMessage();
    }

    private void writeMessage() throws IOException {
        byte[] message = "Client started".getBytes();
        ByteBuffer buffer = ByteBuffer.wrap(message);
        clientSocket.write(buffer);
    }

    ByteBuffer readProcessImageStream() throws IOException {

        ByteBuffer bb = ByteBuffer.allocate(4);
        clientSocket.read(bb);
        ByteBuffer buffer = ByteBuffer.allocate(bb.getInt(0));
        clientSocket.read(buffer);
        System.out.println("NIO BYTE BUFFER SIZE: " + buffer.capacity());
        return buffer;

    }

    private void firstMessage() throws IOException {
        ByteBuffer bb = ByteBuffer.allocate(4);
        clientSocket.read(bb);
        ByteBuffer buffer = ByteBuffer.allocate(bb.getInt(0));
        clientSocket.read(buffer);
        buffer.flip();
        System.out.println("FIRST BYTE BUFFER SIZE: " + buffer.capacity());
        String s = StandardCharsets.UTF_8.decode(buffer).toString();
        System.out.println("MESSAGE: " + s);
        clientSocket.write(bb);
        clientSocket.write(buffer);
    }

}