package tcp;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.Arrays;

public class TCPClient {

    private String ip;
    private int port;
    private Socket clientSocket;
    private DataInputStream inputStream;

    public TCPClient(String ip, int port) {
        this.ip = ip;
        this.port = port;
    }

    public void getDataStream() { // run the service
        try {
            clientSocket = requestServer();
            inputStream = new DataInputStream(clientSocket.getInputStream());
            readFirstMessage();

            while (true) {
                ByteBuffer bb = readProcessImageStream();
                System.out.println("BYTE BUFFER: " + bb);
            }
        } catch (IOException | InterruptedException ex) {
            System.out.println("--- Interrupt server.NetworkService-run");
            System.out.println("SOCKET CLOSED? " + clientSocket.isClosed());
            System.out.println("EXCEPTION: " +ex);

        } finally {
            System.out.println("--- Ende server.NetworkService(pool.shutdown)");
            try {
                if (!clientSocket.isClosed()) {
                    System.out.println("--- Ende server.NetworkService:ServerSocket close");
                    clientSocket.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    Socket requestServer() throws IOException, InterruptedException {
        Socket socket = new Socket(ip, port); // verbindet sich mit Server
        String message = "Client started";
        printMessage(socket, message);

        return socket;

    }

    private void readFirstMessage() throws IOException, InterruptedException {

        int size = inputStream.readInt();
        System.out.println("FIRST MESSAGE SIZE: " + size);
        byte[] msg = new byte[size];
        inputStream.read(msg);
        String s = new String(msg, Charset.defaultCharset());
        System.out.println("FIRST MESSAGE STRING: " + s);
        System.out.println("FIRST MESSAGE ARRAY: " + Arrays.toString(msg));

        printMessage(clientSocket, s);

    }

    void printMessage(Socket socket, String message) throws IOException {
        PrintWriter printWriter =
                new PrintWriter(
                        new OutputStreamWriter(
                                socket.getOutputStream()));
        printWriter.print(message);
        System.out.println("WRITTEN MESSAGE " + message);
        printWriter.flush();
    }

    ByteBuffer readProcessImageStream() throws IOException {

        int size = inputStream.readInt();
        byte[] bytes = new byte[size];
        inputStream.read(bytes);
        ByteBuffer buffer = ByteBuffer.wrap(bytes);
        System.out.println("NIO BYTE BUFFER SIZE: " + buffer.capacity());
        return buffer;

    }

}