import tcp.NIOClient;
import tcp.TCPClient;

public class Main {

    public static void main(String[] args) {
        String serverAddress = args[0];
        String[] address = serverAddress.split(":");
        String ip = address[0];
        int port = Integer.parseInt(address[1]);
        NIOClient client = new NIOClient(ip, port);
        client.getDataStream();
    }
}
